// SPDY-FileCopyrightText: 2013 Canonical Ltd.
// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "contentitem.h"

#include <QFile>
#include <QDir>
#include <QDebug>
#include <QMimeDatabase>

ContentItem::ContentItem(QObject *parent)
    : QObject(parent)
{
}

ContentItem::ContentItem(const QUrl &url, QObject *parent)
    : QObject(parent)
    , m_url(url)
{
}

bool ContentItem::move(const QUrl &dir, const QString &fileName)
{
    QString path(m_url.toLocalFile());

    if (!QFile::exists(path)) {
        qWarning() << "File not found:" << path;
        return false;
    }

    QFileInfo fi(path);
    QDir d(dir.toString());
    if (!d.exists())
        d.mkpath(d.absolutePath());

    QString destFilePath;
    if (fileName.isEmpty())
        destFilePath = dir.toString() + QDir::separator() + fi.fileName();
    else
        destFilePath = dir.toString() + QDir::separator() + fileName;

    if (!QFile::rename(fi.absoluteFilePath(), destFilePath)) {
        qWarning() << "Failed to move content to:" << destFilePath << "falling back to copy";
        if (not QFile::copy(fi.absoluteFilePath(), destFilePath)) {
            qWarning() << "Failed to copy content to:" << destFilePath;
            return false;
        }
    }

    setUrl(QUrl::fromLocalFile(destFilePath));
    return true;
}

bool ContentItem::move(const QUrl &dir)
{
    return move(dir, {});
}

QUrl ContentItem::toDataURI()
{
    QString path(m_url.toLocalFile());

    /* Don't attempt to create the dataUri if the file isn't local */
    if (!QFile::exists(path)) {
        qWarning() << "File not found:" << path;
        return QUrl();
    }
    QMimeDatabase mdb;
    QMimeType mt = mdb.mimeTypeForFile(path);
    /* Don't attempt to create the dataUri if we can't detect the mimetype */
    if (!mt.isValid()) {
        qWarning() << "Unknown MimeType for file:" << path;
        return QUrl();
    }

    QString contentType(mt.name());
    QByteArray data;

    QFile file(path);
    if(file.open(QIODevice::ReadOnly)) {
        data = file.readAll();
        file.close();
    }

    /* Don't attempt to create the dataUri with empty data */
    if (data.isEmpty()) {
        qWarning() << "Failed to read contents of file:" << path;
        return QUrl();
    }

    QString dataUri(QStringLiteral("data:"));
    dataUri.append(contentType);
    dataUri.append(QStringLiteral(";base64,"));
    dataUri.append(QString::fromLatin1(data.toBase64()));
    return QUrl(dataUri);
}

QUrl ContentItem::url() const
{
    return m_url;
}

void ContentItem::setUrl(const QUrl &url)
{
    m_url = url;
    Q_EMIT urlChanged();
}
