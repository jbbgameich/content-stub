// SPDX-FileCopyrightText: 2013 Canonical Ltd.
// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#ifndef COM_UBUNTU_CONTENTTRANSFER_H_
#define COM_UBUNTU_CONTENTTRANSFER_H_

#include "contentstore.h"
#include "contenttype.h"

#include <QList>
#include <QObject>
#include <QQmlListProperty>

class ContentItem;

class ContentTransfer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(State state READ state WRITE setState NOTIFY stateChanged)
    Q_PROPERTY(Direction direction READ direction CONSTANT)
    Q_PROPERTY(SelectionType selectionType READ selectionType WRITE setSelectionType NOTIFY selectionTypeChanged)
    Q_PROPERTY(QString store READ store NOTIFY storeChanged)
    Q_PROPERTY(QQmlListProperty<ContentItem> items READ itemsProperty NOTIFY itemsChanged)
    Q_PROPERTY(QString downloadId READ downloadId WRITE setDownloadId NOTIFY downloadIdChanged)
    Q_PROPERTY(ContentType::Type contentType READ contentType CONSTANT)
    Q_PROPERTY(QString source READ source NOTIFY sourceChanged)
    Q_PROPERTY(QString destination READ destination NOTIFY destinationChanged)

public:
    enum State {
        Created,
        Initiated,
        InProgress,
        Charged,
        Collected,
        Aborted,
        Finalized,
        Downloading,
        Downloaded
    };
    enum Direction {
        Import,
        Export,
        Share
    };
    enum SelectionType {
        Single,
        Multiple
    };
    Q_ENUM(State);
    Q_ENUM(Direction);
    Q_ENUM(SelectionType);

    ContentTransfer(QObject *parent = nullptr);

    State state() const;
    void setState(State state);

    Direction direction() const;

    SelectionType selectionType() const;
    void setSelectionType(SelectionType);

    QQmlListProperty<ContentItem> itemsProperty();
    QList<ContentItem *> items() const;
    void setItems(const QList<ContentItem *> &items);

    Q_INVOKABLE bool start();
    Q_INVOKABLE bool finalize();

    const QString store() const;
    Q_INVOKABLE void setStore(ContentStore *contentStore);

    QString downloadId();
    void setDownloadId(const QString &downloadId);

    ContentType::Type contentType() const;
    void setContentType(const ContentType::Type &contentType);

    QString source() const;
    void setSource(const QString &source);

    QString destination() const;
    void setDestination(const QString &dest);

Q_SIGNALS:
    void stateChanged();
    void itemsChanged();
    void selectionTypeChanged();
    void storeChanged();
    void downloadIdChanged();

    void sourceChanged();
    void destinationChanged();

private:
    QList<ContentItem *> m_items;
    State m_state;
    Direction m_direction;
    SelectionType m_selectionType;

    ContentType::Type m_contentType;
    ContentStore *m_contentStore;
    QString m_source;
    QString m_dest;
    QString m_downloadId;
};

#endif
