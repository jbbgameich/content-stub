// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "contenthub.h"

ContentHub::ContentHub(QObject *parent)
    : QObject(parent)
{
}

ContentHub &ContentHub::instance()
{
    static ContentHub instance;
    return instance;
}
