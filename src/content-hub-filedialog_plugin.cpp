// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: GPL-3.0-only

#include "content-hub-filedialog_plugin.h"

#include "contenthandler.h"
#include "contenthub.h"
#include "contentitem.h"
#include "contentpeer.h"
#include "contentpeermodel.h"
#include "contentscope.h"
#include "contentstore.h"
#include "contenttransfer.h"

#include <QQmlEngine>

void ContentHubFiledialogPlugin::registerTypes(const char *uri)
{
    Q_INIT_RESOURCE(contentstub);

    // @uri Ubuntu.Content
    for (int minor = 0; minor <= 3; minor++) {
        qmlRegisterType<ContentHandler>(uri, 1, minor, "ContentHandler");
        qmlRegisterType<ContentHub>(uri, 1, minor, "ContentHub");
        qmlRegisterType<ContentItem>(uri, 1, minor, "ContentItem");
        qmlRegisterType<ContentPeer>(uri, 1, minor, "ContentPeer");
        qmlRegisterType<ContentPeerModel>(uri, 1, minor, "ContentPeerModel");
        qmlRegisterType(QUrl(QStringLiteral("qrc:/contentstub/ContentPeerPicker.qml")), uri, 1, minor, "ContentPeerPicker");
        qmlRegisterType<ContentScope>(uri, 1, minor, "ContentScope");
        qmlRegisterType<ContentStore>(uri, 1, minor, "ContentStore");
        qmlRegisterType<ContentTransfer>(uri, 1, minor, "ContentTransfer");
        qmlRegisterType(QUrl(QStringLiteral("qrc:/contentstub/ContentTransferHint.qml")), uri, 1, minor, "ContentTransferHint");
        qmlRegisterType<ContentType>(uri, 1, minor, "ContentType");
    }
}
